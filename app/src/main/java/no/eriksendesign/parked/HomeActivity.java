package no.eriksendesign.parked;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import no.eriksendesign.parked.data.CarContract.CarEntry;
import no.eriksendesign.parked.data.CarDbHelper;

import static no.eriksendesign.parked.data.CarContract.CarEntry.TABLE_NAME;

/**
 * @author Dag-Roger Eriksen
 * @author Jonas Kleppe
 * @author Eilif Johansen
 * @date 27.09.2016
 */

public class HomeActivity extends AppCompatActivity implements
        ConnectionCallbacks, OnConnectionFailedListener {

    public final static String EXTRA_LATITUDE = "no.eriksendesign.parked.LATITUDE";
    public final static String EXTRA_LONGITUDE = "no.eriksendesign.parked.LONGITUDE";

    protected static final String TAG = "HomeActivity";

    /** Database helper that will provide us access to the database */
    private CarDbHelper mDbHelper;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    private double latitude;
    private double longitude;

    /**
     * The latitude and longitude that are saved in the database.
     */
    public double savedLatitude;
    public double savedLongitude;

    Button btnDelete;
    Button btnFindCar;
    Button btnSaveCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buildGoogleApiClient();

        btnDelete = (Button)findViewById(R.id.delete_button);
        btnFindCar = (Button)findViewById(R.id.find_car_button);
        btnSaveCar = (Button)findViewById(R.id.save_location_button);

        btnDelete.setVisibility(btnDelete.GONE);
        btnDelete.setEnabled(false);
        btnFindCar.setVisibility(btnFindCar.GONE);
        btnFindCar.setEnabled(false);

        // To access our database, we instantiate our subclass of SQLiteOpenHelper
        // and pass the context, which is the current activity.
        mDbHelper = new CarDbHelper(this);

        DeleteData();
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        displayDatabaseInfo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
        } else {
            Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /** Called when the user clicks the park car button
     *  When the user hits a button, the current latitude and longitude
     *  is sent to the save car location activity
     */
    public void showSaveCarLocation(View view) {
        Intent intent = new Intent(this, SaveCarLocationActivity.class);
        intent.putExtra(EXTRA_LATITUDE, latitude);
        intent.putExtra(EXTRA_LONGITUDE, longitude);
        startActivity(intent);
    }

    /** Called when the user clicks the find car button
     * When the user hits a button, the saved longitude and latitude is sent to google navigation
     */
    public void showFindCarLocation(View view) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + savedLatitude + "," + savedLongitude + "&mode=w");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    private void displayDatabaseInfo() {
        // Create and/or open a database to read from it
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                CarEntry._ID,
                CarEntry.COLUMN_CAR_LATITUDE,
                CarEntry.COLUMN_CAR_LONGITUDE};

        // Perform a query on the car table
        Cursor cursor = db.query(
                TABLE_NAME,   // The table to query
                projection,            // The columns to return
                null,                  // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                   // The sort order

        try {
            // Figure out the index of each column
            int latitudeColumnIndex = cursor.getColumnIndex(CarEntry.COLUMN_CAR_LATITUDE);
            int longitudeColumnIndex = cursor.getColumnIndex(CarEntry.COLUMN_CAR_LONGITUDE);

            if (cursor.moveToFirst())
            {
                savedLatitude = cursor.getDouble(latitudeColumnIndex);
                savedLongitude = cursor.getDouble(longitudeColumnIndex);

                btnDelete.setVisibility(btnDelete.VISIBLE);
                btnDelete.setEnabled(true);
                btnFindCar.setVisibility(btnFindCar.VISIBLE);
                btnFindCar.setEnabled(true);
                btnSaveCar.setVisibility(btnSaveCar.GONE);
                btnSaveCar.setEnabled(false);
            }

        } finally {
            // Always close the cursor when you're done reading from it. This releases all its
            // resources and makes it invalid.
            cursor.close();
        }
    }


    public Integer deleteData () {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        return db.delete(TABLE_NAME, "1", null);
    }

    public void DeleteData() {
        btnDelete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        startActivity(getIntent());
                        Integer deletedRows = deleteData();
                        if(deletedRows > 0)
                            Toast.makeText(HomeActivity.this, R.string.data_reset, Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(HomeActivity.this, R.string.data_not_reset, Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
