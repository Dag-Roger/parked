package no.eriksendesign.parked;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import no.eriksendesign.parked.data.CarContract.CarEntry;
import no.eriksendesign.parked.data.CarDbHelper;

import static no.eriksendesign.parked.HomeActivity.EXTRA_LATITUDE;
import static no.eriksendesign.parked.HomeActivity.EXTRA_LONGITUDE;

/**
 * @author Dag-Roger Eriksen
 * @author Jonas Kleppe
 * @date 29.09.2016
 */

public class SaveCarLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double mapLat; //Current latitude location
    private double mapLong; //Current longitude location

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_car_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        mapLat = intent.getDoubleExtra(EXTRA_LATITUDE, 0.00);
        mapLong = intent.getDoubleExtra(EXTRA_LONGITUDE, 0.00);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker to the current location and zooms the camera in
        LatLng currentLocation = new LatLng(mapLat, mapLong);
        mMap.addMarker(new MarkerOptions()
                .position(currentLocation)
                .title("Current location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15.0f));
    }

    /** Called when the user clicks the park car button */
    public void SaveCarLocation(View view) {
        Intent intent = new Intent(this, HomeActivity.class);
        SaveCarLocationToDatabase();

        startActivity(intent);
    }

    /**
     * Save car location into database.
     */
    private void SaveCarLocationToDatabase() {
        // Create database helper
        CarDbHelper mDbHelper = new CarDbHelper(this);

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a ContentValues object where column names are the keys,
        // and car attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(CarEntry.COLUMN_CAR_LATITUDE, mapLat);
        values.put(CarEntry.COLUMN_CAR_LONGITUDE, mapLong);

        // Insert a new row for car in the database, returning the ID of that new row.
        long newRowId = db.insert(CarEntry.TABLE_NAME, null, values);

        // Show a toast message depending on whether or not the insertion was successful
        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(this, R.string.save_error, Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(this, R.string.save_success, Toast.LENGTH_SHORT).show();
        }
    }
}
