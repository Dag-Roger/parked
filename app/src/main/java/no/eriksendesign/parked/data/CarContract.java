package no.eriksendesign.parked.data;

import android.provider.BaseColumns;

/**
 * @author Dag-Roger Eriksen
 * @date 04.10.2016
 */

public final  class CarContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private CarContract() {}

    /**
     * Inner class that defines constant values for the car database table.
     */
    public static final class CarEntry implements BaseColumns {

        /** Name of database table for car */
        public final static String TABLE_NAME = "car";

        /**
         * Unique ID number for the car (only for use in the database table).
         *
         * Type: INTEGER
         */
        public final static String _ID = BaseColumns._ID;

        /**
         * Latitude coordinates.
         *
         * Type: DOUBLE
         */
        public final static String COLUMN_CAR_LATITUDE ="latitude";

        /**
         * Longitude coordinates.
         *
         * Type: DOUBLE
         */
        public final static String COLUMN_CAR_LONGITUDE = "longitude";
    }
}
